import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://testautomationpractice.blogspot.com/')

WebUI.setText(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/input_First Name_RESULT_TextField-1'), 
    'hola')

WebUI.setText(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/input_Last Name_RESULT_TextField-2'), 
    'mundo')

WebUI.setText(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/input_Phone_RESULT_TextField-3'), 
    '123456789')

WebUI.setText(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/input_Country_RESULT_TextField-4'), 
    'colombia')

WebUI.setText(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/input_City_RESULT_TextField-5'), 
    'bogota')

WebUI.setText(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/input_Email Address_RESULT_TextField-6'), 
    'juan@gmail.com')

WebUI.click(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/label_Male'))

WebUI.click(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/label_Sunday'))

WebUI.click(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/label_Monday'))

WebUI.click(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/label_Tuesday'))

WebUI.click(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/label_Wednesday'))

WebUI.click(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/label_Thursday'))

WebUI.click(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/label_Friday'))

WebUI.click(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/label_Saturday'))

WebUI.selectOptionByValue(findTestObject('Object Repository/34_CallTest/Page_Automation Testing Practice/select_MorningAfternoonEvening'), 
    'Radio-0', true)

