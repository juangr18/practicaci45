<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Select Best MatchStudentDesignerDeve_b45b97</name>
   <tag></tag>
   <elementGuidId>32854cd7-ecaa-4ae3-874b-092e9ae241f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;webtitle&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='webtitle']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>af967362-d6a1-4275-8f62-dbb3131f5f18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>webtitle</value>
      <webElementGuid>5ab79b56-9976-440b-9e58-67f78d055b91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>free-trial-entry</value>
      <webElementGuid>2e781884-ca14-42a0-a7d0-74beaf326d04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>e15d8dc9-9fd5-47c7-bb66-1a7c94bb5ccf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
									Select Best Match
									Student
									Designer
									Developer
									DevOps
									Manager / Director
									Project Manager
									QA
									Other
								</value>
      <webElementGuid>8785b6e3-757a-46f9-8bba-3ceaaae33646</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;signup-form&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col s12 webtitle&quot;]/div[@class=&quot;input-wrap&quot;]/select[@class=&quot;free-trial-entry&quot;]</value>
      <webElementGuid>de98d813-1811-4370-938c-b570d28770fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f071f823-36ac-4ea5-81dd-40ea5ec82bf8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>webtitle</value>
      <webElementGuid>d7bee07f-cdb8-4d37-8175-2455df257282</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>free-trial-entry</value>
      <webElementGuid>d2f27adf-4f8c-44cd-9b8a-8833c378089f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>26d4ff9d-52ad-4bbe-9bf5-62b426d83859</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
									Select Best Match
									Student
									Designer
									Developer
									DevOps
									Manager / Director
									Project Manager
									QA
									Other
								</value>
      <webElementGuid>facbdcbd-0ad7-46ab-8586-54bed4f476b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;signup-form&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col s12 webtitle&quot;]/div[@class=&quot;input-wrap&quot;]/select[@class=&quot;free-trial-entry&quot;]</value>
      <webElementGuid>839fef2c-0693-4989-98eb-b7584b4adfa0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='webtitle']</value>
      <webElementGuid>9ab755d2-7f15-4742-933a-28074cc886c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='signup-form']/div[7]/div/div/select</value>
      <webElementGuid>8683cb54-b5ce-4b09-b972-2ee148b74238</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Job Role is required'])[1]/following::select[1]</value>
      <webElementGuid>a174996b-46c7-488e-b3bd-6c8dbcb923b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What Best Describes You?*'])[1]/following::select[1]</value>
      <webElementGuid>867b28d9-a0d2-4864-a061-b46e3cc92841</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What Type of Testing Do You Want to Accomplish In Your Trial?*'])[1]/preceding::select[1]</value>
      <webElementGuid>cd21fb36-0788-4682-8853-1e1874cab565</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please Complete Form'])[1]/preceding::select[2]</value>
      <webElementGuid>5f882de3-6afd-4f23-9db5-9f63e6dca173</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>6d551a43-4286-4e1b-9ee7-2f311dcd2581</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'webtitle' and (text() = '
									Select Best Match
									Student
									Designer
									Developer
									DevOps
									Manager / Director
									Project Manager
									QA
									Other
								' or . = '
									Select Best Match
									Student
									Designer
									Developer
									DevOps
									Manager / Director
									Project Manager
									QA
									Other
								')]</value>
      <webElementGuid>fcb85c2b-6c66-4666-91db-7bd030edd370</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
