<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create your free account</name>
   <tag></tag>
   <elementGuidId>defe4244-231f-4905-a165-ae9f4c2d1823</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.submit-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick=&quot;$('#signup-form').submit()&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>1b2eda88-fa3f-4b69-8094-6c23626e224c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-btn</value>
      <webElementGuid>3e084521-cc0d-4015-9be7-96a97fc3010a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>$('#signup-form').submit()</value>
      <webElementGuid>318077f6-d1c4-4b63-aa47-bfeca5d3e2ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create your free account</value>
      <webElementGuid>e4e12b2a-87b3-4810-878e-027b732896b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;signup-form&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col s12 text-center&quot;]/a[@class=&quot;submit-btn&quot;]</value>
      <webElementGuid>5f679db4-1b1f-4d90-91da-ebc491defd0d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick=&quot;$('#signup-form').submit()&quot;]</value>
      <webElementGuid>ef561c1c-ae5e-4307-ae06-e86d6006f378</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='signup-form']/div[9]/div/a</value>
      <webElementGuid>1c860b3d-11be-45c0-977f-fe79c4c4b243</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Create your free account')]</value>
      <webElementGuid>b08691ff-588b-4674-a294-ffdb03a2700f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please Complete Form'])[1]/following::a[1]</value>
      <webElementGuid>3a476561-89fd-411c-b42f-d869d2f2b6ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What Type of Testing Do You Want to Accomplish In Your Trial?*'])[1]/following::a[1]</value>
      <webElementGuid>720c4dd0-5721-4b98-aeab-3b0488fdcfb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Privacy Policy'])[1]/preceding::a[1]</value>
      <webElementGuid>4977fd31-0a71-48ef-a2c9-75b4b5ca344e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::a[2]</value>
      <webElementGuid>c72c474b-916c-4faf-815a-84135a056174</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create your free account']/parent::*</value>
      <webElementGuid>6f062e34-6435-41b7-8b10-bd67b6fafdb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/div/a</value>
      <webElementGuid>6e376ba5-6ba5-4cf6-ae42-82e59667c6fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'Create your free account' or . = 'Create your free account')]</value>
      <webElementGuid>09e6556b-1d1e-493b-9d1f-4ef484853d58</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
