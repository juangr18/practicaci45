<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email_userEmail</name>
   <tag></tag>
   <elementGuidId>799ccc83-d8a6-411f-8150-8eaa88fe54c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='userEmail']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#userEmail</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>326bafc9-dca3-4888-8b57-a3a7397b16d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>ca0d86ac-f156-4f76-bc78-ea6c76c78ff0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>name@example.com</value>
      <webElementGuid>d3c6fae2-494a-4b72-a42e-3d17455be6ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>e69bbc7b-8148-41fd-86d0-b231cb3a0145</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>userEmail</value>
      <webElementGuid>c629f5ac-3490-4739-9b15-e502f864a246</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mr-sm-2 form-control</value>
      <webElementGuid>30dda6b3-6635-4ade-b405-45b17aaf8d94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;userEmail&quot;)</value>
      <webElementGuid>4f49957c-4912-40be-8c77-c8eecd706eac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='userEmail']</value>
      <webElementGuid>6ae099c8-d182-4510-9d6d-5d3ceee1e8b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='userEmail-wrapper']/div[2]/input</value>
      <webElementGuid>02ef7923-20f7-4d09-8ad9-ab00c48a2670</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/input</value>
      <webElementGuid>6defe01b-f2c6-45a8-8845-7dfa0cf364de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'name@example.com' and @type = 'email' and @id = 'userEmail']</value>
      <webElementGuid>e4a81024-82c2-43e7-9a93-552eb15b8da5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
