<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_10</name>
   <tag></tag>
   <elementGuidId>8668ce8c-8356-4282-8141-9b43eaff6e5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='cwos']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#cwos</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>3a6de61c-bd2b-47fc-9e5e-b7971d25c6e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jsname</name>
      <type>Main</type>
      <value>VssY5c</value>
      <webElementGuid>db3d1ea5-c4b6-4c77-b475-2cc6e7f211cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>qv3Wpe</value>
      <webElementGuid>07cc2948-6aef-4d8a-8931-0fe831b2338d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>cwos</value>
      <webElementGuid>7ecb6532-32eb-4366-a366-9740cbbb3d35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>e6a3ae55-0c9f-4cd7-a9a5-dd8f1acede8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cwos&quot;)</value>
      <webElementGuid>ceacb900-c22e-406d-9c94-4b5c26f67b03</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='cwos']</value>
      <webElementGuid>7a52f21a-a0e3-4497-ae4c-bfc6e473eba1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='rso']/div/div/div/div/div/div/div/div[2]/div[2]/div/div/span</value>
      <webElementGuid>43af1978-ed42-4560-9eeb-8f2b8ac32453</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tus cálculos y resultados aparecen aquí para que puedas volver a utilizarlos'])[1]/following::span[5]</value>
      <webElementGuid>54c8afc4-3b52-467b-adc1-2cac045b6192</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Resultado de calculadora'])[1]/following::span[6]</value>
      <webElementGuid>ea71cff2-530e-414a-83e9-2f7e96a813df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rad'])[1]/preceding::span[1]</value>
      <webElementGuid>04d12ec1-f639-4c77-973f-4f5ad67b236a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deg'])[1]/preceding::span[1]</value>
      <webElementGuid>c2ef08f3-aadd-4ba7-a016-fa2bf8ba61f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='10']/parent::*</value>
      <webElementGuid>e93345c7-3b26-44ef-b686-4e712c93c38c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div[2]/div[2]/div/div/span</value>
      <webElementGuid>9ac8f295-5962-4056-88cb-5281f2ca2d6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'cwos' and (text() = '10' or . = '10')]</value>
      <webElementGuid>f1e57c25-ade5-4a2a-a2df-3532203ace0c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
