<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_10</name>
   <tag></tag>
   <elementGuidId>6cfb64af-72bc-40aa-af1f-151d20719017</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='rso']/div/div/div/div/div/div/div/div[2]/div[2]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.z7BZJb.XSNERd</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e7722227-4e9e-4eb2-bc1c-0c7ff55a5afb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jsname</name>
      <type>Main</type>
      <value>zLiRgc</value>
      <webElementGuid>09df79da-d346-4860-83fc-4e2b638f065f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>z7BZJb XSNERd</value>
      <webElementGuid>307e0c2e-f726-4a9c-83f0-94ca3ac1adef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>  10 (function(){
var a=document.getElementById(&quot;cwos&quot;),b;var c=parseFloat(a.innerText||a.textContent),d=c.toString();if(12>=d.replace(/^-/,&quot;&quot;).replace(/\./,&quot;&quot;).length)b=d;else if(d=c.toPrecision(12),-1!==d.indexOf(&quot;e&quot;)){var e,f,g,h=null!=(g=null==(e=d.match(/e.*$/))?void 0:null==(f=e[0])?void 0:f.length)?g:0;b=c.toPrecision(12-h-(&quot;0&quot;===d[0]?1:0)).replace(/\.?0*e/,&quot;e&quot;)}else{var k=d.match(/(^-|\.)/g),l=d.substr(0,12+(k?k.length:0));b=-1!==l.indexOf(&quot;.&quot;)?l.replace(/\.?0*$/,&quot;&quot;):l}a.textContent=b;}).call(this); </value>
      <webElementGuid>3675ecba-8cc3-48e3-9f53-f53b2985a51d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rso&quot;)/div[@class=&quot;ULSxyf&quot;]/div[@class=&quot;KIy09e obcontainer wDYxhc NFQFxe&quot;]/div[1]/div[@class=&quot;card-section&quot;]/div[1]/div[@class=&quot;tyYmIf&quot;]/div[@class=&quot;BRpYC&quot;]/div[@class=&quot;TIGsTb lqyc1 d0jRmd&quot;]/div[@class=&quot;fB3vD&quot;]/div[@class=&quot;jlkklc&quot;]/div[@class=&quot;z7BZJb XSNERd&quot;]</value>
      <webElementGuid>52ef7cf7-063d-46e8-9755-c9927132d6d3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='rso']/div/div/div/div/div/div/div/div[2]/div[2]/div/div</value>
      <webElementGuid>69f65592-9c34-4945-9357-699f442f3260</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tus cálculos y resultados aparecen aquí para que puedas volver a utilizarlos'])[1]/following::div[9]</value>
      <webElementGuid>fb8adcf8-0fe4-416b-8bb6-512d4306909d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Resultado de calculadora'])[1]/following::div[15]</value>
      <webElementGuid>4fe7ec70-1854-4590-8a8c-d0374eb7f049</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rad'])[1]/preceding::div[3]</value>
      <webElementGuid>bb6be920-2ba3-4de8-bddb-f0bb39934360</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deg'])[1]/preceding::div[5]</value>
      <webElementGuid>abc56032-44ed-4d89-8568-e2c6e0be5c02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div/div/div/div[2]/div[2]/div/div</value>
      <webElementGuid>28f71dc1-afe3-4bfe-88a9-fd66f8ccec0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '  10 (function(){
var a=document.getElementById(&quot;cwos&quot;),b;var c=parseFloat(a.innerText||a.textContent),d=c.toString();if(12>=d.replace(/^-/,&quot;&quot;).replace(/\./,&quot;&quot;).length)b=d;else if(d=c.toPrecision(12),-1!==d.indexOf(&quot;e&quot;)){var e,f,g,h=null!=(g=null==(e=d.match(/e.*$/))?void 0:null==(f=e[0])?void 0:f.length)?g:0;b=c.toPrecision(12-h-(&quot;0&quot;===d[0]?1:0)).replace(/\.?0*e/,&quot;e&quot;)}else{var k=d.match(/(^-|\.)/g),l=d.substr(0,12+(k?k.length:0));b=-1!==l.indexOf(&quot;.&quot;)?l.replace(/\.?0*$/,&quot;&quot;):l}a.textContent=b;}).call(this); ' or . = '  10 (function(){
var a=document.getElementById(&quot;cwos&quot;),b;var c=parseFloat(a.innerText||a.textContent),d=c.toString();if(12>=d.replace(/^-/,&quot;&quot;).replace(/\./,&quot;&quot;).length)b=d;else if(d=c.toPrecision(12),-1!==d.indexOf(&quot;e&quot;)){var e,f,g,h=null!=(g=null==(e=d.match(/e.*$/))?void 0:null==(f=e[0])?void 0:f.length)?g:0;b=c.toPrecision(12-h-(&quot;0&quot;===d[0]?1:0)).replace(/\.?0*e/,&quot;e&quot;)}else{var k=d.match(/(^-|\.)/g),l=d.substr(0,12+(k?k.length:0));b=-1!==l.indexOf(&quot;.&quot;)?l.replace(/\.?0*$/,&quot;&quot;):l}a.textContent=b;}).call(this); ')]</value>
      <webElementGuid>891a1375-46a1-4b1f-bd35-54a6579b931e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
