<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_5  5</name>
   <tag></tag>
   <elementGuidId>9ab14f3e-db3a-4938-9285-37d1ae17d469</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='rso']/div/div/div/div/div/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.TIGsTb</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b51ccb50-fc13-4a89-a245-35b5023867ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jsname</name>
      <type>Main</type>
      <value>a1lrmb</value>
      <webElementGuid>4a85e6a5-9356-4078-8688-a7bec9ffb2f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>TIGsTb</value>
      <webElementGuid>da8fba01-1e3a-4304-81c2-93a30c740430</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>                  5 + 5 =         10 (function(){
var a=document.getElementById(&quot;cwos&quot;),b;var c=parseFloat(a.innerText||a.textContent),d=c.toString();if(12>=d.replace(/^-/,&quot;&quot;).replace(/\./,&quot;&quot;).length)b=d;else if(d=c.toPrecision(12),-1!==d.indexOf(&quot;e&quot;)){var e,f,g,h=null!=(g=null==(e=d.match(/e.*$/))?void 0:null==(f=e[0])?void 0:f.length)?g:0;b=c.toPrecision(12-h-(&quot;0&quot;===d[0]?1:0)).replace(/\.?0*e/,&quot;e&quot;)}else{var k=d.match(/(^-|\.)/g),l=d.substr(0,12+(k?k.length:0));b=-1!==l.indexOf(&quot;.&quot;)?l.replace(/\.?0*$/,&quot;&quot;):l}a.textContent=b;}).call(this);    </value>
      <webElementGuid>9ca691cd-b2c3-4109-8987-cd0595e06fc1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rso&quot;)/div[@class=&quot;ULSxyf&quot;]/div[@class=&quot;KIy09e obcontainer wDYxhc NFQFxe&quot;]/div[1]/div[@class=&quot;card-section&quot;]/div[1]/div[@class=&quot;tyYmIf&quot;]/div[@class=&quot;BRpYC&quot;]/div[@class=&quot;TIGsTb&quot;]</value>
      <webElementGuid>c5f701fa-d9f1-4294-8e6b-b1d866f688e2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='rso']/div/div/div/div/div/div/div/div[2]</value>
      <webElementGuid>bd54833a-0257-4f9c-8b6a-2ce0b9f25a1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tus cálculos y resultados aparecen aquí para que puedas volver a utilizarlos'])[1]/following::div[4]</value>
      <webElementGuid>39f4db26-ad6a-415e-b8ec-3125276c0e45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Resultado de calculadora'])[1]/following::div[10]</value>
      <webElementGuid>ac63f261-6016-48b4-89e2-d8cf8ec8fc0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rad'])[1]/preceding::div[8]</value>
      <webElementGuid>ebf7620a-c0b1-4dfe-a31f-353a80048475</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deg'])[1]/preceding::div[10]</value>
      <webElementGuid>682c2528-1b96-4cf9-9173-b9ae195fddf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div/div/div/div/div/div/div[2]</value>
      <webElementGuid>6e4e1ae7-817a-472b-89f1-cb776373b81d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '                  5 + 5 =         10 (function(){
var a=document.getElementById(&quot;cwos&quot;),b;var c=parseFloat(a.innerText||a.textContent),d=c.toString();if(12>=d.replace(/^-/,&quot;&quot;).replace(/\./,&quot;&quot;).length)b=d;else if(d=c.toPrecision(12),-1!==d.indexOf(&quot;e&quot;)){var e,f,g,h=null!=(g=null==(e=d.match(/e.*$/))?void 0:null==(f=e[0])?void 0:f.length)?g:0;b=c.toPrecision(12-h-(&quot;0&quot;===d[0]?1:0)).replace(/\.?0*e/,&quot;e&quot;)}else{var k=d.match(/(^-|\.)/g),l=d.substr(0,12+(k?k.length:0));b=-1!==l.indexOf(&quot;.&quot;)?l.replace(/\.?0*$/,&quot;&quot;):l}a.textContent=b;}).call(this);    ' or . = '                  5 + 5 =         10 (function(){
var a=document.getElementById(&quot;cwos&quot;),b;var c=parseFloat(a.innerText||a.textContent),d=c.toString();if(12>=d.replace(/^-/,&quot;&quot;).replace(/\./,&quot;&quot;).length)b=d;else if(d=c.toPrecision(12),-1!==d.indexOf(&quot;e&quot;)){var e,f,g,h=null!=(g=null==(e=d.match(/e.*$/))?void 0:null==(f=e[0])?void 0:f.length)?g:0;b=c.toPrecision(12-h-(&quot;0&quot;===d[0]?1:0)).replace(/\.?0*e/,&quot;e&quot;)}else{var k=d.match(/(^-|\.)/g),l=d.substr(0,12+(k?k.length:0));b=-1!==l.indexOf(&quot;.&quot;)?l.replace(/\.?0*$/,&quot;&quot;):l}a.textContent=b;}).call(this);    ')]</value>
      <webElementGuid>1dad8694-1c5d-46c1-9b10-766613edda47</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
