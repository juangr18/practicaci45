<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Practice Form</name>
   <tag></tag>
   <elementGuidId>9bec028d-2820-48f0-bd8e-0afd2fb50031</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.element-list.collapse.show > ul.menu-list > #item-0 > span.text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='item-0']/span)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>fad1d5fa-04d7-46dc-b8a4-ab0e6c308387</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>52f65e10-93fc-4b7d-bffc-37e00a4e6329</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Practice Form</value>
      <webElementGuid>2b087fbf-fed2-4250-b7d1-659b338a3952</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;body-height&quot;]/div[@class=&quot;container playgound-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 mt-4  col-md-3&quot;]/div[@class=&quot;left-pannel&quot;]/div[@class=&quot;accordion&quot;]/div[@class=&quot;element-group&quot;]/div[@class=&quot;element-list collapse show&quot;]/ul[@class=&quot;menu-list&quot;]/li[@id=&quot;item-0&quot;]/span[@class=&quot;text&quot;]</value>
      <webElementGuid>904230f9-5003-42dc-94a5-4710ea0f9bc7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='item-0']/span)[2]</value>
      <webElementGuid>e9a431d4-97a4-409b-bec0-835c55ed8dfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forms'])[2]/following::span[1]</value>
      <webElementGuid>9f6b3ea1-99be-4102-b4df-ae13bfa9760f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dynamic Properties'])[1]/following::span[3]</value>
      <webElementGuid>88c61ba5-8b5d-4951-b8da-0ce47b45b442</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alerts, Frame &amp; Windows'])[1]/preceding::span[1]</value>
      <webElementGuid>c67c54bf-20ae-4882-87bd-5c9c1b04cfb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Browser Windows'])[1]/preceding::span[3]</value>
      <webElementGuid>3bddbf45-a419-4608-a712-6cd96891ae34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Practice Form']/parent::*</value>
      <webElementGuid>3ad3ceb6-92ce-477d-8597-73dc1b289ad7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/ul/li/span</value>
      <webElementGuid>4ea4e757-140a-4399-8389-6c214eb13c8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Practice Form' or . = 'Practice Form')]</value>
      <webElementGuid>3aa14abc-50a7-42ce-98d6-e68bf0d433af</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
