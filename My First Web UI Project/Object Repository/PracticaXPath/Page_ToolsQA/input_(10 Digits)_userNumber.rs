<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(10 Digits)_userNumber</name>
   <tag></tag>
   <elementGuidId>9d9813d8-f9e0-42e2-937c-9ec72ac411d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#userNumber</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='userNumber']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>1e75a29b-7d3f-48b3-9a71-c8cb2deb035e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>b2c0faca-c492-4abf-96fd-49e98dc6e064</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>pattern</name>
      <type>Main</type>
      <value>\d*</value>
      <webElementGuid>9ee0d7c4-dee0-4b6c-aa5d-e664210ff9ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>minlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>0b763a25-ec1b-47f9-9d4c-0e39f471c9d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>397a8006-234e-440f-8659-41f5fd49c388</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Mobile Number</value>
      <webElementGuid>99744bb6-1b20-42ad-b41b-67d75e2da4d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>9646f5f3-ca1a-48fd-8c9c-2426ac259365</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>userNumber</value>
      <webElementGuid>95f3172a-da84-4aa8-a400-f4caadc113e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value> mr-sm-2 form-control</value>
      <webElementGuid>778d1ec3-792a-4a24-8716-098decbb3f3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;userNumber&quot;)</value>
      <webElementGuid>245c52bb-1542-4f5f-a1c8-061047cec7b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>610a6962-d80e-495a-b3d9-9c381ad3b322</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>9aead9f8-eda6-4321-acea-ee989eff2f0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>pattern</name>
      <type>Main</type>
      <value>\d*</value>
      <webElementGuid>553aafd8-ba58-4d4e-a8f6-d9031b352d6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>minlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>86b35c7e-fe38-4c1c-be2a-5281826900db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>0b1b9e9a-f21a-4a85-be67-e89795f364d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Mobile Number</value>
      <webElementGuid>9d8a0a8f-8b71-4b13-9a17-96e04136f7d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>6193042a-60e0-4b02-a8ae-9afb18f50fb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>userNumber</value>
      <webElementGuid>f3646966-1181-496b-ad56-632f1308c378</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value> mr-sm-2 form-control</value>
      <webElementGuid>d0abacae-0788-41bb-8132-d6f618127e1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;userNumber&quot;)</value>
      <webElementGuid>7f6789e8-5ae8-46bf-b428-d9701b5d97f3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='userNumber']</value>
      <webElementGuid>022d75d9-7229-4da9-994b-6a43302842ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='userNumber-wrapper']/div[2]/input</value>
      <webElementGuid>ef84eead-0265-47f7-8d9b-f0e223ba11a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/input</value>
      <webElementGuid>fd1a7487-5ecf-4203-9ec5-4327c2a0421c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Mobile Number' and @type = 'text' and @id = 'userNumber']</value>
      <webElementGuid>77f8646d-2e30-4ac9-87d2-ef5cde58647b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
