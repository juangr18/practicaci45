<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Name_firstName</name>
   <tag></tag>
   <elementGuidId>aa45051d-1556-4f34-a430-1c0b2d704f37</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#firstName</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='firstName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f1f6fe0b-7a5a-4350-879a-132a528d0860</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>650eb6e4-b202-4d90-b441-1c72b2103ccf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>First Name</value>
      <webElementGuid>793e4f95-d5b8-4710-81b1-7ee7dbc2cd67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>cc7f41f8-ed9f-40cd-93de-6def584e3f2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>firstName</value>
      <webElementGuid>e7789486-7d52-4445-8334-110bcef97c92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value> mr-sm-2 form-control</value>
      <webElementGuid>82e72726-c197-44f1-aa3a-c8cc906e7672</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;firstName&quot;)</value>
      <webElementGuid>37777fa9-27a2-44e3-8ac5-f20e90293d1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b8703d1f-d52c-479c-a259-e6d6232fb578</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>8947e781-58bf-4b2b-9f45-962c3bbcb834</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>First Name</value>
      <webElementGuid>a2590191-b0c3-4aa3-bad1-f35a87fe1509</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>9c21505f-247b-4508-be04-b147fe9d8247</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>firstName</value>
      <webElementGuid>418991ba-d55e-484f-97e7-eab2e06ed5be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value> mr-sm-2 form-control</value>
      <webElementGuid>98de07a9-2eea-4c20-8496-cba4014244c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;firstName&quot;)</value>
      <webElementGuid>833a0b96-479a-4e60-8ae7-1fedc941729b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='firstName']</value>
      <webElementGuid>c5ea3ae3-ded0-40ad-8ec8-01dc3b95898f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='userName-wrapper']/div[2]/input</value>
      <webElementGuid>44b47bdb-3f17-4e3f-a993-9bda0a432b63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>63d43f50-9996-40c4-bb84-416fbd210251</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'First Name' and @type = 'text' and @id = 'firstName']</value>
      <webElementGuid>8221f3ca-6a50-4999-909c-77aae7980050</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
