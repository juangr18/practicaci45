<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Check Box</name>
   <tag></tag>
   <elementGuidId>88179e32-934c-469d-ac2d-467b01a00236</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#item-1 > span.text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='item-1']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5981d76b-8fdb-426d-8696-5a9c1253acb9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>34d1f480-09e5-48f5-bfb0-a6325af8f5b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Check Box</value>
      <webElementGuid>c129f098-0506-4eae-a6bc-77d7fd101703</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;body-height&quot;]/div[@class=&quot;container playgound-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 mt-4  col-md-3&quot;]/div[@class=&quot;left-pannel&quot;]/div[@class=&quot;accordion&quot;]/div[@class=&quot;element-group&quot;]/div[@class=&quot;element-list collapse show&quot;]/ul[@class=&quot;menu-list&quot;]/li[@id=&quot;item-1&quot;]/span[@class=&quot;text&quot;]</value>
      <webElementGuid>917c8131-88e9-4a7c-b8ea-e9acc09d1c49</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='item-1']/span</value>
      <webElementGuid>36b0dc67-5afb-440d-834b-030357c2475e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Text Box'])[1]/following::span[1]</value>
      <webElementGuid>dde7d22e-7a16-4a16-9171-24205ec36f5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elements'])[2]/following::span[2]</value>
      <webElementGuid>307e5b1a-bc43-4716-a468-a68d8128f66d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Radio Button'])[1]/preceding::span[1]</value>
      <webElementGuid>7e19ec11-d506-4551-ac76-70a6845db325</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Web Tables'])[1]/preceding::span[2]</value>
      <webElementGuid>248aee1b-2bfd-49c0-b016-c156ab8d535c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Check Box']/parent::*</value>
      <webElementGuid>4c53604e-8f3d-490e-95c4-1e63efd439b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/span</value>
      <webElementGuid>b8aa13b1-8b60-4453-851c-07d92979626c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Check Box' or . = 'Check Box')]</value>
      <webElementGuid>1b395592-c97e-48dc-9153-347a88420f99</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
