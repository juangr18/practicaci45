<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_A General File_upload</name>
   <tag></tag>
   <elementGuidId>de712bfb-6124-4816-ad05-9f2d2ea73cb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;upload&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='upload']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ce74fe32-0290-42b1-9402-27a083402bd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>c38ef2df-9e66-4b0e-8593-3e0ae7e9d39c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>styled-click-button</value>
      <webElementGuid>e29ac0f3-b27a-47fb-a896-52c2920e4a8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>upload</value>
      <webElementGuid>ba4993f7-b416-4df5-9d3c-c984f712f175</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Upload</value>
      <webElementGuid>9743e8b4-ba64-4886-bdeb-4a640f80a36f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;page-body&quot;]/div[@class=&quot;centered&quot;]/form[1]/div[@class=&quot;form-label&quot;]/input[@class=&quot;styled-click-button&quot;]</value>
      <webElementGuid>704b3dd5-1f2e-4050-9a8a-8e682e699695</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='upload']</value>
      <webElementGuid>cd6f8bd1-c9ff-4ee6-89fb-3c3295fd6c6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>a0c7078f-3f05-42ad-8f4a-188176b5dc06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'upload']</value>
      <webElementGuid>37e38c93-aa37-4162-b37c-0961c723d631</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
