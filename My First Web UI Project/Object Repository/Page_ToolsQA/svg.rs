<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg</name>
   <tag></tag>
   <elementGuidId>c1899c96-e676-4342-b266-a09ef83fc021</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Elements'])[1]/preceding::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>39bf7fe8-1a7c-4236-8354-5b7d0da20d3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>stroke</name>
      <type>Main</type>
      <value>currentColor</value>
      <webElementGuid>6f6dd2c5-b235-423e-bf04-70f7c934c823</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fill</name>
      <type>Main</type>
      <value>currentColor</value>
      <webElementGuid>e3caec73-47ee-4f4e-b2fe-df6b0120818d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>stroke-width</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>c26df85d-66be-47da-a72e-b61a81455b33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 448 512</value>
      <webElementGuid>6b4e24fb-7ccd-4add-ab27-fe79c6cd0a26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>1em</value>
      <webElementGuid>e670e239-5216-4cad-8b05-8a900befdfd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>1em</value>
      <webElementGuid>390b15ff-8948-4f21-a2ff-49f10ccfc022</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>527c9984-e0d0-49a9-8957-df5c0f592756</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;body-height&quot;]/div[@class=&quot;home-content&quot;]/div[@class=&quot;home-body&quot;]/div[@class=&quot;category-cards&quot;]/div[@class=&quot;card mt-4 top-card&quot;]/div[1]/div[@class=&quot;avatar mx-auto white&quot;]/svg[1]</value>
      <webElementGuid>8adc0336-cbdd-44eb-bc29-ff39bcdf262d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elements'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>9f03b243-2b5e-4e13-9f4f-052427d88921</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forms'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>90506577-59d4-4f97-827d-5de9be263f1d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
