<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Tuesday</name>
   <tag></tag>
   <elementGuidId>77f3870d-4fb2-4635-9a5a-a6fdc267ac56</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='q15']/table/tbody/tr[3]/td/label</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>84188574-9ae8-4328-8b46-d42b580d1891</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>RESULT_CheckBox-8_2</value>
      <webElementGuid>a543caeb-e0d5-4f85-bc6e-69a18f1da314</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tuesday</value>
      <webElementGuid>9d576a91-f5cb-4c6e-92b4-5139376799cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;q15&quot;)/table[@class=&quot;inline_grid choices&quot;]/tbody[1]/tr[3]/td[1]/label[1]</value>
      <webElementGuid>d17f6e65-3aa3-47cd-9d81-325b3edde73c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/34_CallTest/Page_Automation Testing Practice/iframe_Automation Testing Practice_frame-on_f39fe2</value>
      <webElementGuid>82e93f71-d8c5-4adc-8bfb-91789161ac5a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='q15']/table/tbody/tr[3]/td/label</value>
      <webElementGuid>894a5ad1-4f05-48f0-86da-bcc59d3965d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Monday'])[1]/following::label[1]</value>
      <webElementGuid>ceec83c0-61bf-47d9-998b-2a23a6e9a6f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sunday'])[1]/following::label[2]</value>
      <webElementGuid>5f77401d-11bc-4853-9db6-fd373b3fb284</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wednesday'])[1]/preceding::label[1]</value>
      <webElementGuid>977777dd-4d16-4653-8691-67608f7588b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Thursday'])[1]/preceding::label[2]</value>
      <webElementGuid>df357b98-4385-4ddf-b525-d4091a051428</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tuesday']/parent::*</value>
      <webElementGuid>b3b6fa3b-aa4f-45b3-b52e-e4fa4014ba2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td/label</value>
      <webElementGuid>e8b37d32-f962-4adb-8fa2-a17579fc21b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Tuesday' or . = 'Tuesday')]</value>
      <webElementGuid>4af40873-a64d-47b6-add9-7b32aa371213</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
