<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Overview_rtMinus rtMinusHover</name>
   <tag></tag>
   <elementGuidId>3c46b200-4a7d-42d9-bbea-af8bd7d43f09</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ctl00_ContentPlaceholder1_RadTreeView1']/ul/li/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.rtMinus.rtMinusHover</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>be7232ad-1753-487c-9e60-fb7a69e228be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>rtMinus rtMinusHover</value>
      <webElementGuid>610d2494-e828-4ce3-8a1c-366d32c372ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ContentPlaceholder1_RadTreeView1&quot;)/ul[@class=&quot;rtUL rtLines&quot;]/li[@class=&quot;rtLI rtFirst rtLast&quot;]/div[@class=&quot;rtOut&quot;]/span[@class=&quot;rtMinus rtMinusHover&quot;]</value>
      <webElementGuid>ac0f4c6a-de28-463f-a32a-bbd8aeef5cde</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ContentPlaceholder1_RadTreeView1']/ul/li/div/span</value>
      <webElementGuid>9e5f74cd-ae0f-4ac9-8a11-f4b7d82194f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/ul/li/div/span</value>
      <webElementGuid>69f21ca4-17cb-4fec-b484-9468353981db</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
