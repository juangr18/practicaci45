<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Drag me to my target</name>
   <tag></tag>
   <elementGuidId>2964fd03-e1d9-4dc5-8cb6-7805aad63c15</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='draggable']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#draggable</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9174fae7-9bab-41a3-8049-f203ecf8adee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>draggable</value>
      <webElementGuid>8bbea414-fd21-47a8-85fa-4338e8388548</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-widget-content ui-draggable ui-draggable-handle</value>
      <webElementGuid>245f8ba9-ac88-4848-8ff6-dd06839d130e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  Drag me to my target
</value>
      <webElementGuid>210347fe-cc2c-4c54-bc38-a6566cea116e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;draggable&quot;)</value>
      <webElementGuid>a2642a9d-4eb2-44b9-bd19-5f2e556e620a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='draggable']</value>
      <webElementGuid>d99d9c66-deae-44e3-8f6a-8526b791df68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='HTML2']/div/div</value>
      <webElementGuid>9ddf6f33-5ef0-4992-8adc-01270d8e1b28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop'])[1]/following::div[2]</value>
      <webElementGuid>7bfc60e3-c365-42cc-a425-90ba670c9e37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy Text'])[1]/following::div[4]</value>
      <webElementGuid>38241801-4fa8-422e-8b89-5d14c128a476</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop Images'])[1]/preceding::div[3]</value>
      <webElementGuid>b801c7da-37d9-4516-b4bf-667edc82da30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mr John'])[1]/preceding::div[3]</value>
      <webElementGuid>ad130a3a-9f45-48a1-bb03-a5e7ca87b98b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div/div[2]/div/div</value>
      <webElementGuid>c4a7ddd7-ee2a-4823-8756-bd97bee67e3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'draggable' and (text() = '
  Drag me to my target
' or . = '
  Drag me to my target
')]</value>
      <webElementGuid>e33adf6c-d2b3-4bda-b2a7-78ed3e5b1481</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
