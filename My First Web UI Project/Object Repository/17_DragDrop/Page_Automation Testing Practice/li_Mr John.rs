<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Mr John</name>
   <tag></tag>
   <elementGuidId>c0e409ca-48ef-4ccd-add9-85658b80f798</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='gallery']/li</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.ui-widget-content.ui-corner-tr.ui-draggable.ui-draggable-handle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>9bce32bf-817b-4506-a258-44337463e0b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle</value>
      <webElementGuid>89ef846b-2bb9-4b68-b8fa-0e6d2c916bef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Mr John
    
 </value>
      <webElementGuid>03647a72-f25e-4cf8-8025-681da96b9561</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gallery&quot;)/li[@class=&quot;ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle&quot;]</value>
      <webElementGuid>df6726f0-26ca-45d2-a570-7585a3acb67e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='gallery']/li</value>
      <webElementGuid>1957fb25-602f-45fa-878d-a9821dd670c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop Images'])[1]/following::li[1]</value>
      <webElementGuid>28aba830-986c-484e-b346-b45434df440f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop'])[1]/following::li[1]</value>
      <webElementGuid>bcf13420-41da-4416-beb8-dc63cb19720b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mary'])[1]/preceding::li[1]</value>
      <webElementGuid>6d961fb4-0b38-4408-bab4-42b49bd22ef3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li</value>
      <webElementGuid>da43328c-6979-4446-95ee-3d117379d3e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
    Mr John
    
 ' or . = '
    Mr John
    
 ')]</value>
      <webElementGuid>75a0999f-713f-4bca-9fe5-9e2477416fd8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
