<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_draggable  width 100px height 100px pad_92e2ca</name>
   <tag></tag>
   <elementGuidId>b63a2b92-78a6-4a71-8775-0ddd26c5ef3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='HTML2']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#HTML2 > div.widget-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6aecce15-cbf4-4078-8039-aacbbd1d8ac6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>widget-content</value>
      <webElementGuid>d7dc4350-4782-4bac-bfec-832575664af1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>



  
  
  #draggable { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }
  #droppable { width: 150px; height: 150px; padding: 0.5em; float: left; margin: 10px; }
  
  
  
  
  $( function() {
    $( &quot;#draggable&quot; ).draggable();
    $( &quot;#droppable&quot; ).droppable({
      drop: function( event, ui ) {
        $( this )
          .addClass( &quot;ui-state-highlight&quot; )
          .find( &quot;p&quot; )
            .html( &quot;Dropped!&quot; );
      }
    });
  } );
  


 

  Drag me to my target

 

  Drop here

  

</value>
      <webElementGuid>6d675efc-b1af-4464-aa2d-5357e59f68c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;HTML2&quot;)/div[@class=&quot;widget-content&quot;]</value>
      <webElementGuid>df92c510-89de-4b6f-94af-aa877bcb26a5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='HTML2']/div</value>
      <webElementGuid>53c867f9-2926-4ed0-b6ac-213e0c28f92a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop'])[1]/following::div[1]</value>
      <webElementGuid>5e19768e-9c45-48b9-81f6-673ba10f68d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy Text'])[1]/following::div[3]</value>
      <webElementGuid>acf17cb8-e0b4-49f8-99bd-fe25c42bc37e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop Images'])[1]/preceding::div[4]</value>
      <webElementGuid>bfff09b5-43bb-4e91-a021-6744193542ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mr John'])[1]/preceding::div[4]</value>
      <webElementGuid>1c120084-806f-4d3d-8b53-c4b0ba96ff16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/aside/div/div[2]/div</value>
      <webElementGuid>1271bdf6-a80b-4108-9c6f-f031a89ce898</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '



  
  
  #draggable { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }
  #droppable { width: 150px; height: 150px; padding: 0.5em; float: left; margin: 10px; }
  
  
  
  
  $( function() {
    $( &quot;#draggable&quot; ).draggable();
    $( &quot;#droppable&quot; ).droppable({
      drop: function( event, ui ) {
        $( this )
          .addClass( &quot;ui-state-highlight&quot; )
          .find( &quot;p&quot; )
            .html( &quot;Dropped!&quot; );
      }
    });
  } );
  


 

  Drag me to my target

 

  Drop here

  

' or . = '



  
  
  #draggable { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }
  #droppable { width: 150px; height: 150px; padding: 0.5em; float: left; margin: 10px; }
  
  
  
  
  $( function() {
    $( &quot;#draggable&quot; ).draggable();
    $( &quot;#droppable&quot; ).droppable({
      drop: function( event, ui ) {
        $( this )
          .addClass( &quot;ui-state-highlight&quot; )
          .find( &quot;p&quot; )
            .html( &quot;Dropped!&quot; );
      }
    });
  } );
  


 

  Drag me to my target

 

  Drop here

  

')]</value>
      <webElementGuid>0a5b4f6c-65b1-4a04-8db7-0c2683f1fc12</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
