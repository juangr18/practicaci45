<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Mary</name>
   <tag></tag>
   <elementGuidId>950e22c0-59a0-4b4f-8df0-c40db7407d45</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='gallery']/li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>cb74cbc4-3ce4-4337-a93b-2780d435db48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle</value>
      <webElementGuid>c7e975e4-dfce-4c15-9880-e8462277a117</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Mary
    
  
</value>
      <webElementGuid>a42034e9-21d6-4d38-ba81-33b1383d1d93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gallery&quot;)/li[@class=&quot;ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle&quot;]</value>
      <webElementGuid>06125355-244e-4622-8278-04b35f00eb31</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='gallery']/li[2]</value>
      <webElementGuid>5b1534a3-7912-41ab-be5c-60adbc2272f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mr John'])[1]/following::li[1]</value>
      <webElementGuid>ca74381e-3e5a-45c5-b852-ab1fe205d61a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop Images'])[1]/following::li[2]</value>
      <webElementGuid>de0b2a35-6774-41af-9e3e-8a00db3d7df3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]</value>
      <webElementGuid>f1abb188-2b16-4b63-a9b6-ee6548e1530c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
    Mary
    
  
' or . = '
    Mary
    
  
')]</value>
      <webElementGuid>ddbbab38-7baa-473f-9020-8a922484c971</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
